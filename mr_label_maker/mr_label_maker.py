#!/usr/bin/env python3

# Copyright © 2020-2022 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from enum import IntEnum
import argparse
import os
import sys
import time

from . import mesa
from .project import GitlabError

class ExitCode(IntEnum):
    SUCCESS = 0
    USER_ERROR = 1
    API_ERROR = 2  # Note: also used by Argparse as usage error
    BUG = 50

def main(argv=sys.argv[1:]) -> ExitCode:
    parser = argparse.ArgumentParser(description='Label issues and merge requests on GitLab')
    parser.add_argument('--dry-run', '-d',
                        action='store_const',
                        const=True,
                        default=False,
                        help="don't apply any changes")
    parser.add_argument('--project', '-p',
                        required=True,
                        choices=['mesa'],
                        type=str,
                        help='set project')
    parser.add_argument('--token', '-t',
                        default=os.environ.get('GITLAB_TOKEN'),
                        type=str,
                        help="set GitLab API token")
    parser.add_argument('--issues', '-i',
                        nargs='?',
                        default=0,
                        const=-1,
                        type=int,
                        help='process issues')
    parser.add_argument('--merge-requests', '-m', '--mrs',
                        nargs='?',
                        default=0,
                        const=-1,
                        type=int,
                        help='process merge requests')
    parser.add_argument('--label', '-l',
                        nargs=1,
                        default=None,
                        type=str,
                        help="search for issues/MRs with this label (default=None)")
    parser.add_argument('--state', '-s',
                        default='opened',
                        choices=['opened', 'closed', 'merged', 'all'],
                        type=str,
                        help="search for issues/MRs in this state (default=opened)")
    parser.add_argument('--ignore-label-history',
                        action='store_const',
                        const=True,
                        default=False,
                        help="process issues with a history of label changes (default=false)")
    parser.add_argument('--poll',
                        default=0,
                        type=int,
                        help='poll GitLab every POLL seconds')

    args = parser.parse_args(argv)

    if args.project == 'mesa':
        proj = mesa.Mesa()
    else:
        print('unhandled project name: ' + args.project)
        return ExitCode.BUG

    if args.token is None:
        print('GitLab API token not set. Use either GITLAB_TOKEN environment variable or --token option.')
        return ExitCode.USER_ERROR

    proj.set_token(args.token)
    proj.set_dry_run(args.dry_run)
    proj.set_label(args.label or [])
    proj.set_state(args.state)
    proj.set_ignore_label_history(args.ignore_label_history)

    try:
        proj.connect()
    except GitlabError as e:
        print(e)
        return ExitCode.API_ERROR

    while True:
        if args.issues:
            proj.process_issues(args.issues)

            if args.merge_requests:
                print('\n\n')

        if args.merge_requests:
            proj.process_mrs(args.merge_requests)

        if args.poll == 0:
            break
        time.sleep(args.poll)

    return ExitCode.SUCCESS

if __name__ == '__main___':
    exit_code = main()
    if exit_code != ExitCode.SUCCESS:
        sys.exit(exit_code)